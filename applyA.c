/*
    Polynomially-preconditioned Krylov Solver Scalable Toy
    Copyright (C) 2015 Patrick Sanan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <petscdmda.h>
#include "ctx.h"

#undef __FUNCT__
#define __FUNCT__ "applyA"
PetscErrorCode applyA(Mat A, Vec in, Vec out)
{
  PetscErrorCode ierr;
  Ctx *ctx; 
  const PetscScalar **inarr;
  PetscScalar val,**outarr;
  PetscInt i,j,ixs,iys,ixm,iym,imin,imax,jmin,jmax,M,N;
  PetscBool left,right,up,down;
  Vec in_local;
  PetscReal oneoverhy2,oneoverhx2;

  PetscFunctionBeginUser;

  ierr = MatShellGetContext(A, &ctx);CHKERRQ(ierr);
  in_local    = ctx->work_local[0];
  oneoverhy2  = 1.0/(ctx->hy*ctx->hy); 
  oneoverhx2  = 1.0/(ctx->hx*ctx->hx);
  M           = ctx->M;
  N           = ctx->N;

  /* Scatter global-->local to have access to the required ghost values */
  ierr=DMGlobalToLocalBegin(ctx->da,in,INSERT_VALUES,in_local);CHKERRQ(ierr);
  ierr=DMGlobalToLocalEnd  (ctx->da,in,INSERT_VALUES,in_local);CHKERRQ(ierr);

  /* Get the boundaries of the local subdomain */
  DMDAGetCorners(ctx->da, &ixs, &iys, 0, &ixm, &iym, 0);CHKERRQ(ierr);

  /* Get access to the raw arrays (with ghosts). 
     Note that PETSc allows these to be accessed with *global* indices */
  DMDAVecGetArray(ctx->da,out,&outarr);CHKERRQ(ierr);
  DMDAVecGetArrayRead(ctx->da,in_local,&inarr);CHKERRQ(ierr);

  /* Determine active (global) boundaries */
  up   = (iys       == 0); jmin = up   ? iys       + 1 : iys;
  down = (iys + iym == N); jmax = down ? iys + iym - 1 : iys + iym; 
  left = (ixs       == 0); imin = left ? ixs       + 1 : ixs; 
  right= (ixs + ixm == M); imax = right? ixs + ixm - 1 : ixs + ixm;

  /* Handle corners */
  if(up && left){
    val=0;
    j=0; i=0;
    val+=inarr[j  ][i+1] *       (-oneoverhx2);
    val+=inarr[j+1][i  ] *       (-oneoverhy2);
    val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
    outarr[j][i]=val;
  }
  if(up && right){
    val=0;
    j=0; i=M-1;
    val+=inarr[j  ][i-1] *       (-oneoverhx2);
    val+=inarr[j+1][i  ] *       (-oneoverhy2);    
    val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
    outarr[j][i]=val;
  }
  if(down && left){
    val=0;
    j=N-1; i=0;
    val+=inarr[j-1][i  ] *       (-oneoverhy2);
    val+=inarr[j  ][i+1] *       (-oneoverhx2);
    val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
    outarr[j][i]=val;
  }
  if(down && right){
    val=0;
    j=N-1; i=M-1;
    val+=inarr[j-1][i  ] *       (-oneoverhy2);
    val+=inarr[j  ][i-1] *       (-oneoverhx2);
    val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
    outarr[j][i]=val;
  }

  /* Handle edges (excluding corners ) */
  if (up){
    j=0;
    for (i=imin; i<imax; ++i) {
      val=0;
      val+=inarr[j  ][i-1] *       (-oneoverhx2);
      val+=inarr[j  ][i+1] *       (-oneoverhx2);
      val+=inarr[j+1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
      outarr[j][i]=val;
    }
  }

  if (down){
    j = N-1;
    for (i=imin; i<imax; ++i) {
      val=0;
      val+=inarr[j-1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i-1] *       (-oneoverhx2);
      val+=inarr[j  ][i+1] *       (-oneoverhx2);
      val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
      outarr[j][i]=val;
    }
  }

  if (left){
    i = 0;
    for (j=jmin; j<jmax; ++j) {
      val=0;
      val+=inarr[j-1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i+1] *       (-oneoverhx2);
      val+=inarr[j+1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
      outarr[j][i]=val;
    }
  }

  if (right){
    i = M-1;
    for (j=jmin; j<jmax; ++j) {
      val=0;
      val+=inarr[j-1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i-1] *       (-oneoverhx2);
      val+=inarr[j+1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
      outarr[j][i]=val;
    }
  }

  /* Handle (branch-free) the interior points.
     This is the loop nest to optimize with PLuTo */
  for (j=jmin; j<jmax; ++j) {
    for (i=imin; i<imax; ++i) {
      val=0;
      val+=inarr[j-1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i-1] *       (-oneoverhx2);
      val+=inarr[j  ][i+1] *       (-oneoverhx2);
      val+=inarr[j+1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
      outarr[j][i]=val;
    }
  }

  /* Revoke access to raw arrays */
  DMDAVecRestoreArray(ctx->da,out,&outarr);CHKERRQ(ierr);
  DMDAVecRestoreArrayRead(ctx->da,in_local,&inarr);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}
