# Scalable Example for testing high arithmetic intensity polynomial preconditioned Krylov methods

This repository holds a simple PETSc application which solves the constant-coefficient Poisson equation 

    \nabla^2 u = f

with a simple 5-point finite difference scheme, on a rectangular domain, with homogeneous Dirichlet boundary conditions.

It accomplishes this by discretizing the system to (implicitly) define the linear system `Ax=b`, and then solves the equivalent system

    p_m(A)x = q_{m-1}(A)Ax = q_{m-1}b 

with the conjugate gradient method. `p_m(A)` is a Chebyshev polynomial of order `m`, tuned with the eigenvalues of `A`.

## License 
See LICENSE.txt and source files.

## Citations
If you use or modify this code, please cite this repository and the following preprint:

    Donfack, S., Sanan, P., Schenk, O., Reps, B., and Vanroose, W., 
    "A High Arithmetic Intensity Krylov Subspace Method Based on Stencil Compiler Programs"
    [in review]

## Usage
Install and test an optimized build of the latest maintenance release of PETSc. This might be as simple as

    git clone https://bitbucket.org/petsc/petsc -b maint
    cd petsc
    ./configure ./configure --with-cc=gcc --with-cxx=g++ --with-fc=gfortran --download-fblaslapack --download-mpich --with-debugging=0
    <..follow instructions..>
    export PETSC_DIR=$PWD
    export PETSC_ARCH=<..insert PETSC_ARCH value, for example arch-linux-c-opt>

Otherwise, see `./configure --help`, ask @psanan, or read more on the PETSc website:
http://www.mcs.anl.gov/petsc/documentation/installation.html

Note: if you would like to use more aggressive optimization, add `COPTFLAGS='-O3' CXXOPTFLAGS='-O3'` to the `./configure ...` line above.

Return to this directory, build, and test

    cd <..this dir..>
    make
    make test

Examine any differences with the reference output to make sure that they can be attributed to floating point error.

Basic usage might be like
  
    $PETSC_DIR/bin/petscmpiexec -n <NUMPROCS> ./runme -ksp_view -ksp_monitor -ksp_converged_reason -da_processors_x <PROCX> -da_processors_y <PROCY> -da_grid_x <GRIDX> -da_grid_y <GRIDY> -order <m> -kx <KX> -ky <KY>

where `KX` and `KY` control the coefficents `kx` and `ky` in  the initial guess 

    x_0(x,y) = sin(pi * kx * x) * sin(pi * ky * y), 0 < x < 1, 0 < y < 1

Provide `-zeroin` to ignore these and use a zero initial guess. (Note poor notation here, reusing `x`)

See the included help for more available options

    ./runme -help

## Usage on Emmy

* https://www.rrze.fau.de/dienste/arbeiten-rechnen/hpc/systeme/emmy-cluster.shtml
* https://www.rrze.fau.de/dienste/arbeiten-rechnen/hpc/systeme/hpc-environment.shtml

Note: much of the below would more efficiently be done with batch jobs.

Log into the Erlangen cluster and into emmy
 
    ssh path.to.erlangen
    ssh emmy

On emmy obtain this code

    cd /somewhere/to/put/this
    git clone https://psanan@bitbucket.org/psanan/polykrylovpetscexample.git

On emmy, obtain PETSc

    cd somewhere/to/install
    git clone -b maint https://bitbucket.org/petsc/petsc petsc-maint
    cd petsc-maint

Configure PETSc with the included `arch-emmy.py`. Note that this downloads a
suboptimal BLAS/LAPACK. To use the more efficient MKL versions available, 
choose your settings by editing `arch-emmy.py`.

    module load intel64
    python /path/to/here/arch-emmy.py

Interactively run the conftest job

    qsub -I
    <wait>
    module load intel64
    cd /path/to/petsc-maint
    mpirun_rrze -np 1 conftest-arch-emmy
    exit

Run the generated reconfigure script
 
    python reconfigure-arch-emmy.py

Test interactively

    qsub -I
    <wait>
    module load intel64
    export PETSC_DIR=/path/to/petsc-maint/
    export PETSC_ARCH=arch-emmy
    cd $PETSC_DIR/src/snes/examples/tutorials
    make clean 
    make ex19
    mpirun -np 1 ./ex19 -da_refine 3 -snes_monitor_short -pc_type mg -ksp_type fgmres -pc_mg_type full
    mpirun -np 2 ./ex19 -da_refine 3 -snes_monitor_short -pc_type mg -ksp_type fgmres -pc_mg_type full

Output should resemble

    lid velocity = 0.0016, prandtl # = 1, grashof # = 1
      0 SNES Function norm 0.0406612
      1 SNES Function norm 3.33636e-06
      2 SNES Function norm 1.653e-11
    Number of SNES iterations = 2

Build and test this code interactively

    qsub -I
    <wait> 
    module load intel64
    export PETSC_DIR=/path/to/petsc-maint/
    export PETSC_ARCH=arch-emmy
    cd /path/to/here
    make
    mpirun_rrze -np 1 ./runme -da_grid_x 5 -da_grid_y 5 -ksp_monitor -ksp_converged_reason
    mpirun_rrze -np 9 ./runme -da_grid_x 15 -da_grid_y 29 -order 2 -ksp_monitor -ksp_converged_reason
    exit
