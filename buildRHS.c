/*
    Polynomially-preconditioned Krylov Solver Scalable Toy
    Copyright (C) 2015 Patrick Sanan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <petscdmda.h>
#include "ctx.h"

/* Note : no significant effort has been made to optimize this routine */
PetscErrorCode buildRHS(Vec b,Ctx * ctx)
{
  PetscErrorCode ierr;
  PetscScalar **barr;
  PetscInt i,j,ixs,ixm,iys,iym;
  PetscReal dx,dy; 

  PetscFunctionBeginUser;

  /* Apply a function on the interior of the unit square. That is, the first grid
     point in a given direction with N points has coordinate 1/(N+1) and the last 
     has coordinate N/(N+1) */
  ierr = DMDAVecGetArray(ctx->da,b,&barr);CHKERRQ(ierr);
  DMDAGetCorners(ctx->da, &ixs, &iys, 0, &ixm, &iym, 0);CHKERRQ(ierr);
  for(j=iys;j<iys+iym;++j){
    for(i=ixs;i<ixs+ixm;++i){
      dx = (PetscReal)(i+1)/(ctx->M+1.0) - 0.5;
      dy = (PetscReal)(j+1)/(ctx->N+1.0) - 0.5;
      barr[j][i] = PetscExpReal(-100.0 * ((dx * dx) + (dy * dy)));
    }
  }
  ierr = DMDAVecRestoreArray(ctx->da,b,&barr);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
