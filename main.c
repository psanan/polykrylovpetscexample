/*
    Polynomially-preconditioned Krylov Solver Scalable Toy
    Copyright (C) 2015 Patrick Sanan and Simplice Donfack

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
static char help[] = 
"Solve the Poisson problem on the interior of the unit square, with homogeneous Dirichlet\n\
boundary conditions, using polynomial preconditioning\n\
-da_grid_x <nx> : the number of interior grid points in the x direction\n\
-da_grid_y <ny> : the number of interior grid points in the y direction\n\
-order <m>      : the order of the polynomial to use\n\
-kx             : eigenvalue number for the initial guess\n\
-ky             : eigenvalue number for the initial guess\n\
-shift <s>      : shift for the lower eigenvalue estimate\n\
-zeroin         : ignore -kx and -ky and use a zero initial guess\n\
-debug1         : use an assembled operator P=I\n\
-debug1         : use a matrix-free P=I\n\
-debug3         : use a naive implementaton of P\n\
-dump           : save some system data in octave/matlab format (slow)\n\
\n\n";

#include <petscksp.h>
#include <petscdm.h>
#include <petscdmda.h>

/* A context which we pass to our matrix-free operator 
   We do nothing to follow proper OOP practices. */
#include "ctx.h"

/* A function which applies A, matrix free
   Note: you can see the entries of the equivalent operator (slow) 
   with the -ksp_view_mat_explicit option */
extern PetscErrorCode applyA(Mat A,Vec in,Vec out);

/* A function to apply P . This is set up to be optimized by PLuTo */
extern PetscErrorCode applyP(Mat A,Vec in,Vec out);

/* A function to apply P naively (repeatedly applying the matrix-free operator) */
extern PetscErrorCode applyPnaive(Mat A,Vec in,Vec out);

/* A function to apply Q naively (repeatedly applying the matrix-free operator) */
extern PetscErrorCode applyQnaive(Mat A,Vec in,Vec out);

/* TODO: A function to apply Q, matrix-free efficiently
extern PetscErrorCode applyQ(Mat A, Vec in, Vec out); */

/* Populate an assembled version of A, for comparison and debugging */
extern PetscErrorCode assembleA(Mat A,Ctx *ctx);

/* A function which returns estimates to be used to define Chebyshev polynomials */
extern PetscErrorCode obtainEigenvalueEstimates(PetscReal *lmin,PetscReal *lmax, Ctx * ctx);

/* A function to build the right hand side */
extern PetscErrorCode buildRHS(Vec b,Ctx * ctx);

/* A function to build the initial guess */
extern PetscErrorCode buildInitialGuess(Vec x,Ctx * ctx,PetscScalar kx,PetscScalar ky);

/* A function which, in a somewhat hacky manner, creates a local vector
   with redundant storage which can be "viewed" (see applyP.c)
   as a 3d array, instead of a 2d array, thus allowing for effective PLUTO 
   tiling! */
extern PetscErrorCode DMCreateLocalVectorBuffered(DM da,Vec* g,PetscScalar ** a);

/*****************************************************************************/
#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  Ctx            ctx;
  Mat            A,Adb,Pnaive,P;
  Vec            Qb,b,tmp,x;
  KSP            ksp;
  PC             pc;
  PetscBool      set,zeroin,debug1,debug2,debug3,dump;
  PetscInt       i,procs_x,procs_y;
  PetscReal      norm,Qbnorm,bnorm;
  PetscScalar    kx,ky;

  PetscInitialize(&argc,&argv,(char*)0,help);

  /* Process command line arguments */
  ierr = PetscOptionsGetInt(NULL,NULL,"-order",&ctx.order,&set);CHKERRQ(ierr);
  if(!set) { ctx.order = 1; }
  ierr = PetscOptionsGetScalar(NULL,NULL,"-kx",&kx,&set);CHKERRQ(ierr);
  if(!set) { kx = 5; }
  ierr = PetscOptionsGetScalar(NULL,NULL,"-ky",&ky,&set);CHKERRQ(ierr);
  if(!set) { ky = 5; }
  ierr = PetscOptionsGetReal(NULL,NULL,"-shift",&ctx.interval_shift,&set);CHKERRQ(ierr);
  if(!set) { ctx.interval_shift=0.0 ;}
  ierr = PetscOptionsGetBool(NULL,NULL,"-zeroin",&zeroin,&set);CHKERRQ(ierr);
  if(!set) { zeroin = PETSC_FALSE; }
  ierr = PetscOptionsGetBool(NULL,NULL,"-dump",&dump,&set);CHKERRQ(ierr);
  if(!set){ dump = PETSC_FALSE; }
  ierr = PetscOptionsGetBool(NULL,NULL,"-debug1",&debug1,&set);CHKERRQ(ierr);
  if(!set){ debug1 = PETSC_FALSE; }
  ierr = PetscOptionsGetBool(NULL,NULL,"-debug2",&debug2,&set);CHKERRQ(ierr);
  if(!set){ debug2 = PETSC_FALSE; }
  ierr = PetscOptionsGetBool(NULL,NULL,"-debug3",&debug3,&set);CHKERRQ(ierr);
  if(!set){ debug3 = PETSC_FALSE; }
  if ((PetscInt)(debug1)+(PetscInt)(debug2)+(PetscInt)(debug3) > 1){
    SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_SUP, "Only one of the debug flags can be used");
  }

  /* Set up a Distributed Array Domain Manager (DMDA) 
    Note that the negative numbers imply that the sizes can and should be set from
    the command line (see the examples in the makefile) */
  {
  const DMDAStencilType stencil = ctx.order > 1 ? DMDA_STENCIL_BOX : DMDA_STENCIL_STAR; 
  ierr = DMDACreate2d(
    PETSC_COMM_WORLD, 
    DM_BOUNDARY_NONE,DM_BOUNDARY_NONE, /* No boundary ghost nodes, since we use zero Dirichlet BCs */
    stencil,                           /* The stencil we consider is itself a star, but there is no 
                                          "fat star" stencil that we can use to represent iterated 
                                           star stencils, so we use the less-efficient box */
    -3,-3,                             /* Global sizes. The negative values indicate that these 
                                          can be altered from the command line */
    PETSC_DECIDE,PETSC_DECIDE,         /* Local sizes, determined automatically */
    1,                                 /* Scalar Problem */
    ctx.order,                         /* Stencil width is the same as the order of the polynomial */
    NULL,NULL,                         /* Use the default layout */
    &ctx.da);CHKERRQ(ierr);
   }

  /* Define constant grid spacing, under the assumption of the domain being the _interior_
    of the unit square */
  ierr  = DMDAGetInfo(ctx.da,0,&ctx.M,&ctx.N,0,&procs_x,&procs_y,0,0,0,0,0,0,0);CHKERRQ(ierr);
  if(ctx.M < 2 || ctx.N < 2) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"M and N must be at least 2");
  ctx.hx = 1.0/(PetscScalar)(ctx.M+1); 
  ctx.hy = 1.0/(PetscScalar)(ctx.N+1);

  /* TODO: assert that the domains are twice as wide as the overlap. This is a restriction of the way we have coded applyP. 
     Note that  you can actually get away with a higher order if you have no floating domains (say order 30 on 4 procs on a 100x100 domain).
     We issue this error anyway.
    */
  if(2 * ctx.order > PetscMin(ctx.M/procs_x,ctx.N/procs_y)){ 
    SETERRQ5(PETSC_COMM_WORLD,PETSC_ERR_SUP,"order (%d) is too high for this mesh partitioning ( %d by %d dof on %d by %d procs).",ctx.order,ctx.M,ctx.N,procs_x,procs_y);CHKERRQ(ierr);
  }
  
  /* Use the Domain Manager to create vectors
     with the appropriate parallel layout. 
     The 'local' vector contains ghost values
     These are not all used in all modes*/
  ierr = DMCreateGlobalVector(ctx.da,&Qb);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)Qb,"Q(A)b");CHKERRQ(ierr);
  ierr = VecDuplicate(Qb,&x);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)x,"x");CHKERRQ(ierr);
  ierr = VecDuplicate(Qb,&b);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)b,"b");CHKERRQ(ierr);
  ierr = VecDuplicate(Qb,&tmp);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)tmp,"tmp");CHKERRQ(ierr);
#ifdef PKDEMO_USE_BUFFER
  for(i=0;i<4;++i){ /* TODO: check mode and only allocate what is needed */
#else
  for(i=0;i<3;++i){ /* TODO: check mode and only allocate what is needed */
#endif
      /* Note that this allocates memory which we must manually free later */
#ifdef PKDEMO_USE_BUFFER
    ierr = DMCreateLocalVectorBuffered(ctx.da,&ctx.work_local[i],&ctx.work_local_a[i]);CHKERRQ(ierr);
#endif
    ierr = DMCreateLocalVector(ctx.da,&ctx.work_local[i]);CHKERRQ(ierr);
  }
  for(i=0;i<3;++i){  /* TODO: check mode and only allocate what is needed */
    ierr = VecDuplicate(Qb,&ctx.work[i]);CHKERRQ(ierr);
  }

  /* An assembled matrix for debugging and verification */
  if(debug1){
    ierr = DMSetMatType(ctx.da, MATAIJ);CHKERRQ(ierr);
    ierr = DMCreateMatrix(ctx.da,&Adb);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)Adb,"Adb");
    ierr = assembleA(Adb,&ctx);CHKERRQ(ierr);
  }

  /* Use the Domain Manager to create an operator (Mat) and vectors (Vec)
     with the appropriate parallel layout */

  /* A applies the operator once, matrix-free */
  ierr = DMSetMatType(ctx.da, MATSHELL);CHKERRQ(ierr);
  ierr = DMCreateMatrix(ctx.da,&A);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)A,"A");
  ierr = MatShellSetContext(A, &ctx);CHKERRQ(ierr);
  ierr = MatShellSetOperation(A,MATOP_MULT,(void(*)(void))applyA);CHKERRQ(ierr);
  ctx.A = A; /*Used to construct the manufactured solution, and used for the naive polynomial */

  /* An operator for debugging which applies P naively, without using the full halo allocated,
     performing a halo exchange after each matrix multiply */
  if(debug3){
    ierr = DMSetMatType(ctx.da, MATSHELL);CHKERRQ(ierr);
    ierr = DMCreateMatrix(ctx.da,&Pnaive);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)Pnaive,"Pnaive");
    ierr = MatShellSetContext(Pnaive,&ctx);CHKERRQ(ierr);
    ierr = MatShellSetOperation(Pnaive,MATOP_MULT,(void(*)(void))applyPnaive);CHKERRQ(ierr);
  }

  /* An operator which applies the polynomial P_m(A), where m = order */
  ierr = DMSetMatType(ctx.da,MATSHELL);CHKERRQ(ierr);
  ierr = DMCreateMatrix(ctx.da,&P);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)P,"P");
  ierr = MatShellSetContext(P,&ctx);CHKERRQ(ierr);
  ierr = MatShellSetOperation(P,MATOP_MULT,(void(*)(void))applyP);CHKERRQ(ierr);

  /* Create and set up the linear solver context */
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  if(debug1){
    ierr = KSPSetOperators(ksp,Adb,Adb);CHKERRQ(ierr);
  }else if (debug2){
    ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
  }else if(debug3){
    ierr = KSPSetOperators(ksp,Pnaive,Pnaive);CHKERRQ(ierr);
  }else{
    ierr = KSPSetOperators(ksp,P,P);CHKERRQ(ierr);
  }
  ierr = KSPSetType(ksp,KSPCG);CHKERRQ(ierr); /* CG */
  ierr = KSPSetInitialGuessNonzero(ksp,!zeroin);CHKERRQ(ierr); /* Set flag for nonzero initial guess */

   /* Increase Divergence tolerance to a huge value, since this operator gets very poorly conditioned
     and set the relative convergence to 1e-8 */
  ierr = KSPSetTolerances(ksp,1e-8,PETSC_DEFAULT,1e99,PETSC_DEFAULT);CHKERRQ(ierr);

  /* Use a single-reduction (Chronopoulos-Gear) variant of CG
  ierr = KSPCGUseSingleReduction(KSP,PETSC_TRUE);CHKERRQ(ierr); */

  /* No Preconditioner */
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  ierr = PCSetType(pc,PCNONE);CHKERRQ(ierr); /* No PC */

  /* Obtain extreme eigenvalue estimates.
     Note that for small systems these can be verified with LAPACK by using 
     -debug1 or -debug2 so that the system operator is simply A, and
     -ksp_compute_eigenvalues_explicitly */
  ierr = obtainEigenvalueEstimates(&ctx.lmin,&ctx.lmax,&ctx);CHKERRQ(ierr);
  if(ctx.interval_shift==0.0){
    ierr = PetscPrintf(PETSC_COMM_WORLD,
      "Computed Spectral Interval for A: (%f,%f)\n",ctx.lmin,ctx.lmax);CHKERRQ(ierr);
  }else{
    ierr = PetscPrintf(PETSC_COMM_WORLD,
        "Shifted, Computed Spectral Interval: (%f,%f)\n",ctx.lmin,ctx.lmax);CHKERRQ(ierr);
  }

  /* Populate the right hand side and compute Q(A)b  */
  ierr = buildRHS(b,&ctx);CHKERRQ(ierr);
  ierr = applyQnaive(P,b,Qb);CHKERRQ(ierr);
  /* TODO: an efficient application of Q */

  /* Set initial guess */
  if(!zeroin){ ierr=buildInitialGuess(x,&ctx,kx,ky);CHKERRQ(ierr); } 

  /* Set from options. You can see options by running with -help */
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);

  /* Compute the relative norm of the residual b - A*x */
  ierr = MatMult(A,x,tmp);CHKERRQ(ierr);
  ierr = VecAXPBY(tmp,1.0,-1.0,b);CHKERRQ(ierr);
  ierr = VecNorm(tmp,NORM_2,&norm);
  ierr = VecNorm(b,NORM_2,&bnorm);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Initial ||b-Ax||/||b||: %g\n",norm/bnorm);CHKERRQ(ierr);

  /* Compute the relative norm of the preconditioned residual Q(A)b-P(A)m */
  ierr = MatMult(P,x,tmp);CHKERRQ(ierr);
  ierr = VecAXPBY(tmp,1.0,-1.0,Qb);CHKERRQ(ierr);
  ierr = VecNorm(tmp,NORM_2,&norm);
  ierr = VecNorm(Qb,NORM_2,&Qbnorm);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Initial ||Q(A)b-P(A)x||/||Q(A)b||: %g\n",norm/Qbnorm);CHKERRQ(ierr);

  /* Print squared norms of b and b*/
  ierr = PetscPrintf(PETSC_COMM_WORLD,"||b||^2 = %g \t ||Q(A)b||^2 = %g\n",bnorm*bnorm,Qbnorm*Qbnorm);CHKERRQ(ierr);

  /* Run */
  if(!(debug1 || debug2)){
    ierr = KSPSolve(ksp,Qb,x);CHKERRQ(ierr);
  }else{
    ierr = KSPSolve(ksp,b,x);CHKERRQ(ierr);
  }

  /* Debug: dump the operator and vectors to be opened in Octave (only for small sizes) */
  if (dump) {
    Mat Op,Opex,Aex;
    const char *name;
    PetscViewer viewer;
    ierr = PetscPrintf(PETSC_COMM_WORLD, "Dumping vectors and explicit operator to file...\n");CHKERRQ(ierr);
    ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,"dump.m",&viewer);CHKERRQ(ierr);
    PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_MATLAB);CHKERRQ(ierr);
    ierr = KSPGetOperators(ksp,&Op,NULL);CHKERRQ(ierr);
    ierr = PetscObjectGetName((PetscObject)Op,&name);CHKERRQ(ierr);
    ierr = MatComputeExplicitOperator(Op,&Opex);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)Opex,name);CHKERRQ(ierr);
    ierr = MatView(Opex,viewer);CHKERRQ(ierr);
    ierr = MatDestroy(&Opex);
    ierr = MatComputeExplicitOperator(A,&Aex);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)Aex,"Aex");CHKERRQ(ierr);
    ierr = MatView(Aex,viewer);CHKERRQ(ierr);
    ierr = MatDestroy(&Aex);
    ierr = VecView(x,viewer);CHKERRQ(ierr);
    ierr = VecView(Qb,viewer);CHKERRQ(ierr);
    ierr = VecView(b,viewer);CHKERRQ(ierr);
    {
      Mat Q,Qex;
      ierr = DMSetMatType(ctx.da, MATSHELL);CHKERRQ(ierr);
      ierr = DMCreateMatrix(ctx.da,&Q);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject)Q,"Q");
      ierr = MatShellSetContext(Q, &ctx);CHKERRQ(ierr);
      ierr = MatShellSetOperation(Q,MATOP_MULT,(void(*)(void))applyQnaive);CHKERRQ(ierr);
      ierr = MatComputeExplicitOperator(Q,&Qex);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject)Qex,"Qex");CHKERRQ(ierr);
      ierr = MatView(Qex,viewer);CHKERRQ(ierr);
      ierr = MatDestroy(&Qex);
      ierr = MatDestroy(&Q);CHKERRQ(ierr);
    }
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "Finished dumping explicit operator to file.\n");CHKERRQ(ierr);
  }

  /* Compute the residual b - A*x */
  ierr = MatMult(A,x,tmp);CHKERRQ(ierr);
  ierr = VecAXPBY(tmp,1.0,-1.0,b);CHKERRQ(ierr);
  ierr = VecNorm(tmp,NORM_2,&norm);
  ierr = VecNorm(b,NORM_2,&bnorm);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"||b-Ax||/||b||: %g\n",norm/bnorm);CHKERRQ(ierr);

  /* Compute the relative norm of the preconditioned residual Q(A)b-P(A)x */
  ierr = MatMult(P,x,tmp);CHKERRQ(ierr);
  ierr = VecAXPBY(tmp,1.0,-1.0,Qb);CHKERRQ(ierr);
  ierr = VecNorm(tmp,NORM_2,&norm);
  ierr = VecNorm(Qb,NORM_2,&Qbnorm);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"||Q(A)b-P(A)x||/||Q(A)b||: %g\n",norm/Qbnorm);CHKERRQ(ierr);

  /* Free all allocated objects */
                    ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  if(debug1){       ierr = MatDestroy(&Adb);CHKERRQ(ierr);}
                    ierr = MatDestroy(&A);CHKERRQ(ierr);
  if(debug3){       ierr = MatDestroy(&Pnaive);CHKERRQ(ierr);}
                    ierr = VecDestroy(&Qb);CHKERRQ(ierr);
                    ierr = VecDestroy(&x);CHKERRQ(ierr);
                    ierr = VecDestroy(&tmp);CHKERRQ(ierr);
#ifdef PKDEMO_USE_BUFFER
  for(i=0;i<4;++i){ 
#else
  for(i=0;i<3;++i){ 
#endif
   ierr = VecDestroy(&ctx.work_local[i]);CHKERRQ(ierr); /*TODO: only allocate these as needed */
#ifdef PKDEMO_USE_BUFFER
   PetscFree(ctx.work_local_a[i]);
#endif
  }
  for(i=0;i<3;++i){ ierr = VecDestroy(&ctx.work[i]);CHKERRQ(ierr);}       /*TODO: only allocate these as needed */
  ierr = DMDestroy(&ctx.da);CHKERRQ(ierr);

  /* Finalize */
  PetscFinalize();

  return 0;
}
