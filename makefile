#
#   Polynomially-preconditioned Krylov Solver Scalable Toy
#   Copyright (C) 2015 Patrick Sanan
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#   
#   


# We use PETSc's included variables and rules,
#  so you must have PETSC_DIR and PETSC_ARCH
#  defined to refer to a working PETSc build
#
# Note that PETSc defines a "clean" target in its rules,
#  which are otherwise convenient to use, so to clean here,
#  use the "allclean" target.

# define this  to 1 to copy all test output to the corresponding reference file (Careful!)
COPY_TEST_OUTPUT=0
# e.g. to update all test output (Careful!) you could do
#   make COPY_TEST_OUTPUT=1 test
# This will produce the diffs, and then copy.

EXNAME=runme
OBJ=main.o applyA.o assembleA.o obtainEigenvalueEstimates.o applyPnaive.o applyP.o \
    buildRHS.o applyQnaive.o buildInitialGuess.o bufferedVectors.o
HEADERDEPS=ctx.h

ALL : $(EXNAME)

# These take care of providing variables to link to PETSc
#  and rules for building .o files from .c files
include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

# Add a flag to allow allocation of extra space to ease data-dependencies
#CFLAGS+=-DPKDEMO_USE_BUFFER

$(OBJ) : $(HEADERDEPS)

$(EXNAME): $(OBJ) chkopts
	-${CLINKER} -o $@ $(OBJ) ${PETSC_LIB}

clean :: 
	rm -f $(EXNAME) $(OBJ)

# A set of tests, which can be used as examples 
# [remove the "@" from the line with ${MPIEXEC} and the command will be displayed, and you can then copy and modify it]
# Note that if there are significant differences from the reference, you will likely overflow an output buffer. 
# In that case, run the tests individually, or comment out the "@rm -f" lines and examine the testXXX.tmp files and/or 
#  compare them with testref/testXXX.ref
test : run_test1  \
       run_test2  \
       run_test3  \
       run_test4  \
       run_test5  \
       run_test6  \
       run_test7  \
       run_test8  \
       run_test9  \
       run_test10 \
       run_test11 \
       run_test12 \
       run_test13 \
       run_test14 \
       run_test15 \
       run_test16 \
       run_test17 \
       run_test18 \
       run_test19 \
       run_test20 \
       run_test21 \
       run_test22 \
       run_test23 \

# Standard PETSc options to get information about the solver and solve
TEST_OPTS=-ksp_view -ksp_monitor -ksp_converged_reason
TEST_OPTS_VERBOSE=-ksp_view -ksp_monitor_true_residual -ksp_converged_reason

# TODO: These should be wrapped in a function to avoid duplication
# The only interesting differences are on the ${MPIEXEC} lines

run_test1: ${EXNAME}
	@rm -f test1.tmp
	@echo "\033[34mRunning Test 1\033[0m"
	@${MPIEXEC} -n 1 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 5  -da_grid_y 5 -debug2 \
    2>&1 > test1.tmp
	@diff test1.tmp testref/test1.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test1.tmp testref/test1.ref; fi;
	@rm -f test1.tmp

run_test2: ${EXNAME}
	@rm -f test2.tmp
	@echo "\033[34mRunning Test 2\033[0m"
	@${MPIEXEC} -n 4 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 5  -da_grid_y 5 -debug2 \
    2>&1 > test2.tmp
	@diff test2.tmp testref/test2.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test2.tmp testref/test2.ref; fi;
	@rm -f test2.tmp

run_test3: ${EXNAME}
	@rm -f test3.tmp
	@echo "\033[34mRunning Test 3\033[0m"
	@${MPIEXEC} -n 9 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 40 -da_grid_y 33 -debug2 \
    2>&1 > test3.tmp
	@diff test3.tmp testref/test3.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test3.tmp testref/test3.ref; fi;
	@rm -f test3.tmp

# The matrix free operator, otherwise as in test 3
run_test4: ${EXNAME}
	@rm -f test4.tmp
	@echo "\033[34mRunning Test 4\033[0m"
	@${MPIEXEC} -n 9 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 40 -da_grid_y 33 -debug1 \
    2>&1 > test4.tmp
	@diff test4.tmp testref/test4.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test4.tmp testref/test4.ref; fi;
	@rm -f test4.tmp

# The naive operator with order 1
# This should converge like 1 and 2, but the operator and RHS are scaled, so 
#  the residuals differ
run_test5: ${EXNAME}
	@rm -f test5.tmp
	@echo "\033[34mRunning Test 5\033[0m"
	@${MPIEXEC} -n 1 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 5  -da_grid_y 5 -debug3 \
    2>&1  > test5.tmp
	@diff test5.tmp testref/test5.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test5.tmp testref/test5.ref; fi;
	@rm -f test5.tmp

# The naive operator with order 1 on a non-square grid
run_test6: ${EXNAME}
	@rm -f test6.tmp
	@echo "\033[34mRunning Test 6\033[0m"
	@${MPIEXEC} -n 9 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 40 -da_grid_y 33 -debug3 \
    2>&1 > test6.tmp
	@diff test6.tmp testref/test6.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test6.tmp testref/test6.ref; fi;
	@rm -f test6.tmp

# The operator with order 1
# Should behave like 1, 2 (but the norm will differ)
#  and be identical to 5
run_test7: ${EXNAME}
	@rm -f test7.tmp
	@echo "\033[34mRunning Test 7 \033[0m"
	@${MPIEXEC} -n 1 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 5 -da_grid_y 5 \
    2>&1 > test7.tmp
	@diff test7.tmp testref/test7.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test7.tmp testref/test7.ref; fi;
	@rm -f test7.tmp

# The operator with order 1 on a non-square grid
# Should behave like 3, 4 (but the norm will differ)
# and should be very similar to 6 (it is, but there are noticeable numerical differences)
run_test8: ${EXNAME}
	@rm -f test8.tmp
	@echo "\033[34mRunning Test 8\033[0m"
	@${MPIEXEC} -n 9 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 40 -da_grid_y 33 \
    2>&1 > test8.tmp
	@diff test8.tmp testref/test8.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test8.tmp testref/test8.ref; fi;
	@rm -f test8.tmp

# The naive operator on the small grid with order 2
run_test9: ${EXNAME}
	@rm -f test9.tmp
	@echo "\033[34mRunning Test 9\033[0m"
	@${MPIEXEC} -n 1 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 5 -da_grid_y 5 -order 2 -debug3 \
    2>&1 > test9.tmp
	@diff test9.tmp testref/test9.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test9.tmp testref/test9.ref; fi;
	@rm -f test9.tmp

# The operator on a small grid with order 2
run_test10: ${EXNAME}
	@rm -f test10.tmp
	@echo "\033[34mRunning Test 10\033[0m"
	@${MPIEXEC} -n 1 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 5 -da_grid_y 5 -order 2 \
    2>&1 > test10.tmp
	@diff test10.tmp testref/test10.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test10.tmp testref/test10.ref; fi;
	@rm -f test10.tmp

# The naive operator on a single processor, with a square grid, with higher order
run_test11: ${EXNAME}
	@rm -f test11.tmp
	@echo "\033[34mRunning Test 11\033[0m"
	@${MPIEXEC} -n 1 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 20 -da_grid_y 20 -order 7 -debug3 \
    2>&1 > test11.tmp
	@diff test11.tmp testref/test11.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test11.tmp testref/test11.ref; fi;
	@rm -f test11.tmp
   
# The operator on a single processor, with a square grid, with higher order
# The output should be the same as test 11
run_test12: ${EXNAME}
	@rm -f test12.tmp
	@echo "\033[34mRunning Test 12\033[0m"
	@${MPIEXEC} -n 1 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 20 -da_grid_y 20 -order 7 \
    2>&1 > test12.tmp
	@diff test12.tmp testref/test12.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test12.tmp testref/test12.ref; fi;
	@rm -f test12.tmp
   
# The naive operator on 9 processes, on a square grid, with order 3
run_test13: ${EXNAME}
	@rm -f test13.tmp
	@echo "\033[34mRunning Test 13\033[0m"
	@${MPIEXEC} -n 9 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 29 -da_grid_y 29 -order 3 -debug3 \
    2>&1 > test13.tmp
	@diff test13.tmp testref/test13.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test13.tmp testref/test13.ref; fi;
	@rm -f test13.tmp
   
# The operator on 9 processes, on a square grid, with order 3
#  the output should be the same as test 13
run_test14: ${EXNAME}
	@rm -f test14.tmp
	@echo "\033[34mRunning Test 14\033[0m"
	@${MPIEXEC} -n 9 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 29 -da_grid_y 29 -order 3 \
    2>&1 > test14.tmp
	@diff test14.tmp testref/test14.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test14.tmp testref/test14.ref; fi;
	@rm -f test14.tmp
   
# The naive operator on a non-square grid, with multiple processes and order 3
run_test15: ${EXNAME}
	@rm -f test15.tmp
	@echo "\033[34mRunning Test 15\033[0m"
	@${MPIEXEC} -n 9 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 40 -da_grid_y 33 -debug3 -order 3 \
    2>&1 > test15.tmp
	@diff test15.tmp testref/test15.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test15.tmp testref/test15.ref; fi;
	@rm -f test15.tmp

# The operator on a non-square grid, with multiple processes and order 3
#  should compare to test 15
run_test16: ${EXNAME}
	@rm -f test16.tmp
	@echo "\033[34mRunning Test 16\033[0m"
	@${MPIEXEC} -n 9 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 40 -da_grid_y 33 -order 3 \
    2>&1 > test16.tmp
	@diff test16.tmp testref/test16.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test16.tmp testref/test16.ref; fi;
	@rm -f test16.tmp

# very high order on a square grid and several MPI procs with the naive operator
run_test17: ${EXNAME}
	@rm -f test17.tmp
	@echo "\033[34mRunning Test 17\033[0m"
	@${MPIEXEC} -n 4 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 100 -da_grid_y 100 -debug3 -order 24 \
    2>&1 > test17.tmp
	@diff test17.tmp testref/test17.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test17.tmp testref/test17.ref; fi;
	@rm -f test17.tmp

# very high order on a square grid and several MPI procs
# Compare test 17
run_test18: ${EXNAME}
	@rm -f test18.tmp
	@echo "\033[34mRunning Test 18\033[0m"
	@${MPIEXEC} -n 4 ./${EXNAME} ${TEST_OPTS} -zeroin -da_grid_x 100 -da_grid_y 100 -order 24 \
    2>&1 > test18.tmp
	@diff test18.tmp testref/test18.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test18.tmp testref/test18.ref; fi;
	@rm -f test18.tmp

run_test19: ${EXNAME}
	@rm -f test19.tmp
	@echo "\033[34mRunning Test 19\033[0m"
	@${MPIEXEC} -n 20 ./${EXNAME} ${TEST_OPTS} -zeroin -da_processors_x 4 -da_processors_y 5 -da_grid_x 134 -da_grid_y 177 -order 6 -debug3 \
    2>&1 > test19.tmp
	@diff test19.tmp testref/test19.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test19.tmp testref/test19.ref; fi;
	@rm -f test19.tmp

#Compare test 19
run_test20: ${EXNAME}
	@rm -f test20.tmp
	@echo "\033[34mRunning Test 20\033[0m"
	@${MPIEXEC} -n 20 ./${EXNAME} ${TEST_OPTS} -zeroin -da_processors_x 4 -da_processors_y 5 -da_grid_x 134 -da_grid_y 177 -order 6 \
    2>&1 > test20.tmp
	@diff test20.tmp testref/test20.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test20.tmp testref/test20.ref; fi;
	@rm -f test20.tmp

# A test using the sinusoidal initial guess 
run_test21: ${EXNAME}
	@rm -f test21.tmp
	@echo "\033[34mRunning Test 21\033[0m"
	@${MPIEXEC} -n 9 ./${EXNAME} ${TEST_OPTS}  -da_grid_x 51 -da_grid_y 51 -order 5 -kx 5 -ky 5 \
 >  test21.tmp
	@diff test21.tmp testref/test21.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test21.tmp testref/test21.ref; fi;
	@rm -f test21.tmp

# A test using the sinusoidal initial guess 
run_test22: ${EXNAME}
	@rm -f test22.tmp
	@echo "\033[34mRunning Test 22\033[0m"
	@${MPIEXEC} -n 9 ./${EXNAME} ${TEST_OPTS} -da_grid_x 40 -da_grid_y 33 -order 3 \
> test22.tmp
	@diff test22.tmp testref/test22.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test22.tmp testref/test22.ref; fi;
	@rm -f test22.tmp

# This is designed to exactly reproduce the behavior of a reference code. NOTE that in that reference code, 
#   there was a parameter to shift the bottom eigenvalue used in the polynomial, which was turned off.
# The reference code run with the following parameters should produce output which can be directly compared
#   to the settings below (see testref/test23.ref for the output)
# ./main 4 16 1000
# Note that this reference code is included with the commit that introduced this example for convenience,
#   but it is not intended for any other purpose. Find it in reference_code/
run_test23: ${EXNAME}
	@rm -f test23.tmp
	@echo "\033[34mRunning Test 23\033[0m"
	@${MPIEXEC} -n 1 ./${EXNAME} ${TEST_OPTS_VERBOSE} -da_grid_x 16 -da_grid_y 16 -order 4 -shift 0.33333333333333333 \
> test23.tmp
	@diff test23.tmp testref/test23.ref && \
    echo "\033[32mSuccess\033[0m" || \
    echo "\033[31mFailure: output does not match reference (see diff above)\033[0m"
	@if [ ${COPY_TEST_OUTPUT} -eq 1 ] ; then cp test23.tmp testref/test23.ref; fi;
	@rm -f test23.tmp

.PHONY: ALL        \
        allclean   \
        test       \
        run_test1  \
        run_test2  \
        run_test3  \
        run_test4  \
        run_test5  \
        run_test6  \
        run_test7  \
        run_test8  \
        run_test9  \
        run_test10 \
        run_test11 \
        run_test12 \
        run_test13 \
        run_test14 \
        run_test15 \
        run_test16 \
        run_test17 \
        run_test18 \
        run_test19 \
        run_test20 \
        run_test21 \
        run_test22 \
        run_test23 \

