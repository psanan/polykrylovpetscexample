#!/usr/bin/env python

# Module load intel64 
# to load intel mpi

configure_options = [
  '--with-cc=mpicc',
  '--with-cxx=mpicxx',
  '--with-fc=0',

  # for ease of testing, download and build BLAS/LAPACK [inefficient!]
  '--download-f2cblaslapack'

  # 64 bit (sequential?)
  #'--COPTFLAGS="-O3 -DMKL_ILP64 -fopenmp -m64','--with-blas-lapack-dir=/opt/intel/composer_xe_2015/mkl/lib/intel64'

  # MKL threads 64 bit
  #'--COPTFLAGS="-O3 -DMKL_LP64 -fopenmp -m64','--with-blas-lapack-lib="-Wl,--no-as-needed -L/opt/intel/mkl/lib/intel64 -lmkl_intel_lp64 -lmkl_core -lmkl_gnu_thread -lpthread -lm"'

  # 32 bit
  #'--COPTFLAGS="-O3 -fopenmp -m32','--with-blas-lapack-lib="-Wl,--no-as-needed -L/opt/intel/mkl/lib/ia32 -lmkl_intel -lmkl_core -lmkl_gnu_thread -lpthread -lm"'

  '--with-clib-autodetect=0',
  '--with-cxxlib-autodetect=0',
  '--with-fortranlib-autodetect=0',

  '--with-shared-libraries=0',
  '--with-debugging=0',
  '--with-valgrind=0',

  '--with-batch',
  '--known-mpi-shared-libraries=1',

  '--with-x=0',

  'PETSC_ARCH=arch-emmy'
  ]

if __name__ == '__main__':
  import sys,os
  sys.path.insert(0,os.path.abspath('config'))
  import configure
  configure.petsc_configure(configure_options)
